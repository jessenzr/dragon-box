package com.dragonbox.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.dragonbox.state.MainState;

public class DragonBox extends ApplicationAdapter {
	MainState state;
	
	public DragonBox() {
		state = new MainState();
	}
	
	@Override
	public void create () {
		state.create();
	}

	@Override
	public void render () {
		float delta = Gdx.graphics.getDeltaTime();
		
		state.update(delta);
		state.render();
	}
}
