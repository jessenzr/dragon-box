package com.dragonbox.state;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.dragonbox.helpers.FontManager;
import com.dragonbox.input.MyInputHandler;

public class MainState {
	private World world;
	private FontManager font;
	private OrthographicCamera cam;
	private Box2DDebugRenderer debug;	
	
	private Vector2 gravity;
	private Vector2 camSize;
	
	private float accumulator;
	private float worldStep = 1/180f;
	
	private int tipo = 1;
	private int tamanho = 1;
	
	public void create() {
		gravity = new Vector2(0, -10);
		camSize	= new Vector2(10, 10);
		
        world = new World(gravity, true);
        
        createCamera();
        createInput();
        
        debug = new Box2DDebugRenderer();
        
        font = new FontManager("fonts/dejavu");
        
        createPlatform();
	}
	
	public int getTipo() {
		return this.tipo;
	}
	
	public void trocaEscala(boolean aumenta) {
		if (aumenta)
			if (this.tamanho == 5)
				this.tamanho = 1;
			else
				this.tamanho++;
		else
			if (this.tamanho == 1)
				this.tamanho = 5;
			else
				this.tamanho--;
	}
	
	public void trocaTipo(boolean aumenta) {
		if (aumenta)
			if (this.tipo == 3)
				this.tipo = 1;
			else
				this.tipo++;
		else
			if (this.tipo == 1)
				this.tipo = 3;
			else
				this.tipo--;
	}
	
	public void createForm(float x, float y) {
		switch (this.tipo) {
		case 1:
			createBox(x, y);
			break;
		case 2:
			createCircle(x, y);
			break;
		case 3:
			createTriangle(x, y);
			break;
		default:
			break;
		}
	}
	
	public void createBox(float x, float y) {
		BodyDef bd = new BodyDef();
        bd.type = BodyType.DynamicBody;
        
        x = ((x * camSize.x) / Gdx.graphics.getWidth());
        y = (((Gdx.graphics.getHeight() - y) * camSize.y) / 
        		Gdx.graphics.getHeight());
        
        bd.position.set(x, y);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(tamanho * .2f, tamanho *.2f, 
        			   new Vector2((tamanho * .2f) / 2, 0), 0);

        world.createBody(bd).createFixture(shape, 0);
	}
	
	public void createTriangle(float x, float y) {
		BodyDef bd = new BodyDef();
        bd.type = BodyType.DynamicBody;
        
        x = ((x * camSize.x) / Gdx.graphics.getWidth());
        y = (((Gdx.graphics.getHeight() - y) * camSize.y) / 
        		Gdx.graphics.getHeight());
        
        bd.position.set(x, y);
        
        Vector2 vert[] = { new Vector2(-1 * tamanho, 0), 
        				   new Vector2(1 * tamanho, 0), 
        				   new Vector2(0, 2 * tamanho) };
        
        PolygonShape shape = new PolygonShape();
        shape.set(vert);

        world.createBody(bd).createFixture(shape, 0);
	}
	
	public void createCircle(float x, float y) {
		BodyDef bd = new BodyDef();
        bd.type = BodyType.DynamicBody;
        
        x = ((x * camSize.x) / Gdx.graphics.getWidth());
        y = (((Gdx.graphics.getHeight() - y) * camSize.y) / 
        		Gdx.graphics.getHeight());
        
        bd.position.set(x, y);

        CircleShape shape = new CircleShape();
        shape.setRadius((float) Math.sqrt(Math.pow(tamanho * .2f, 2) + 
        						  Math.pow(tamanho * .2f, 2)));
        world.createBody(bd).createFixture(shape, 0);
	}
	
	private void createPlatform() {
		BodyDef bd = new BodyDef();
        bd.type = BodyType.StaticBody;
        bd.position.set(0, 0.2f);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(camSize.x, 0.2f, new Vector2(camSize.x / 2, 0), 0);
        
        world.createBody(bd).createFixture(shape, 0);
	}
	
	private void createInput() {
		InputMultiplexer input = new InputMultiplexer();
        MyInputHandler mih = new MyInputHandler(this);
        GestureDetector gesture = new GestureDetector(mih);
        input.addProcessor(mih);
        input.addProcessor((InputProcessor) gesture);
        Gdx.input.setInputProcessor(input);
	}
	
	private void createCamera() {
		cam = new OrthographicCamera();
        cam.setToOrtho(false, camSize.x, camSize.y);
        cam.update();
	}
	
	public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 0);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        debug.render(world, cam.combined);

        String str = "";
        switch (this.tipo) {
		case 1:
			str = "quadrado";
			break;
		case 2:
			str = "circulo";
			break;
		case 3:
			str = "triangulo";
			break;
		default:
			break;
		}
        font.draw("Tamanho: " + this.tamanho, 460, 470);
        font.draw("Tipo: " + str, 460, 450);
	}
	
	public void update(float delta) {
		float frameTime = Math.min(delta, 0.25f);
        accumulator += frameTime;
        while (accumulator >= worldStep) {
            world.step(worldStep, 8, 100);
            accumulator -= worldStep;
        }
		
		cam.update();
	}
}
