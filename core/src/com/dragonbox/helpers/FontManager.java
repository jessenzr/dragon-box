package com.dragonbox.helpers;

import java.util.concurrent.Callable;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.BitmapFontData;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ArrayMap;

public class FontManager {

    private ArrayMap<Vector2, String> strings;
    private ArrayMap<Vector2, Callable<Integer>> functions;
    private BitmapFont font;
    private SpriteBatch batch;
    private static GlyphLayout glyph  = new GlyphLayout();

    public FontManager(String fontFile) {
        float scale = Gdx.graphics.getHeight() *
        		Gdx.graphics.getHeight() / Gdx.graphics.getWidth();
        strings = new ArrayMap<Vector2, String>();
        functions = new ArrayMap<Vector2, Callable<Integer>>();
        font = new BitmapFont(Gdx.files.internal(fontFile + ".fnt"),
                              Gdx.files.internal(fontFile + ".png"), false);
        font.getData().setScale(scale / 600);

        batch = new SpriteBatch();
    }

    public void addFont(Vector2 pos, String string) {
        glyph.setText(font, string);
        pos.x -= glyph.width / 2f;
        pos.y += glyph.height / 2f;
        strings.put(pos, string);
    }

    public void addFunc(Vector2 pos, Callable<Integer> callable) {
        functions.put(pos, callable);
    }
    
    public void draw(String str, float posx, float posy) {
    	batch.begin();
    	font.draw(batch, str, posx, posy);
        batch.end();
    }

	public void draw() {
        batch.begin();
        for (Vector2 pos : strings.keys()) {
            String str = strings.get(pos);
            font.draw(batch, str, pos.x, pos.y);
        }

        for (Vector2 pos : functions.keys()) {
            Callable<Integer> func = functions.get(pos);
            try {
                font.draw(batch, String.valueOf(func.call()), pos.x, pos.y);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        batch.end();
    }

    public BitmapFontData getFontData() {
        return font.getData();
    }

    public BitmapFont getFont() {
        return font;
    }
}
